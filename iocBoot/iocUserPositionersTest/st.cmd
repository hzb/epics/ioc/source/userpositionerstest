#!../../bin/linux-x86_64/UserPositionersTest

< envPaths
epicsEnvSet("ENGINEER","Luca Porzio")

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/UserPositionersTest.dbd"
UserPositionersTest_registerRecordDeviceDriver pdbbase

## Load record instances

#var streamDebug 1

cd "${TOP}/iocBoot/${IOC}"
iocInit

dbl > /opt/epics/ioc/log/logs.dbl
